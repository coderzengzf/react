import { useState } from 'react'

const home = () => {
    const [count, setCount] = useState(0)

    function handleClick() {
        setCount(count + 1)
    }

    return(
        <>
            <div>
                <h1>这是home页面</h1>
                <button onClick={handleClick}>
                    home点击次数: {count}
                </button>
            </div>
        </>
    )
}

export default home
