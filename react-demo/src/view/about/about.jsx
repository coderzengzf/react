import { useState } from 'react'

const about = () => {
    const [count, setCount] = useState(0)

    function handleClick() {
        setCount(count + 1);
    }

    return(
        <>
            <div>
                <h1>这是about页面</h1>
                <button onClick={handleClick}>
                    about点击次数: {count}
                </button>
            </div>
        </>
    )
}

export default about
