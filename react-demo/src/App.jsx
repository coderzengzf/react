import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import { HashRouter } from "react-router-dom";
// import AuthRouter from "@/routers/utils/authRouter";
import React from 'react'
import { NavLink, Route, Routes } from 'react-router-dom'
import Home from './view/home/home.jsx'
import About from './view/about/about.jsx'
import './App.css'

// const titleList = [
//     {title: 'home页面', path: '/home'},
//     {title: 'about页面', path: '/about'},
// ]

const App = (props) => {
    // function handlePage(path){
    //     console.log('path', path)
    // }
    return (
        <>
            <div>
                {/*<div className="title">*/}
                {/*    {titleList.map(item => {*/}
                {/*        return <div onClick={handlePage(item.path)} key={item.title}>{item.title}</div>;*/}
                {/*    })}*/}
                {/*</div>*/}
                <nav>
                    <ul>
                        <li><NavLink to="/home" className={({ isActive }) =>
                            isActive ? "link-active" : undefined
                        }>Home</NavLink></li>
                        <li><NavLink to="/about" className={({ isActive }) =>
                            isActive ? "link-active" : undefined
                        }>About</NavLink></li>
                    </ul>
                </nav>
                <Routes>
                    {/* 路由匹配住/home后，不会再往下匹配 */}
                    <Route path="/home" element={<Home/>}></Route>
                    <Route path="/about" element={<About/>}></Route>
                </Routes>
            </div>
        </>
    );
};

export default App
