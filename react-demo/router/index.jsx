import { Navigate, useRoutes} from "react-router-dom";
import Home from '../src/view/home/home.jsx'
import About from '../src/view/about/about.jsx'
import React from "react";

export const rootRouter = [
    {
        path: "/",
        element: <Home />,
        meta: {
            requiresAuth: false,
            title: "首页",
            key: "home"
        }
    },
    {
        path: "/about",
        element: React.lazy("@/view/about/about.jsx"),
        meta: {
            requiresAuth: false,
            title: "关于页面",
            key: "about"
        }
    },
    {
        path: "*",
        element: <Navigate to="/404" />
    }
];

const Router = () => {
    const routes = useRoutes(rootRouter);
    return routes;
};

export default Router;
